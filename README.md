# Russian Olympiada Poster

I made a nice poster template using LuaLaTeX and BibTeX.

# Basic Usage
If you don't know about LaTeX or BibTeX or don't want to install a lot of packages, I would reccomend using [Overleaf](https://www.overleaf.com/), which should handle most of the complicated things for you.
The only files you will need to edit is `poster.tex` and `bibliography.bib`.
